﻿using Shared.Models;
using Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DesktopApp.Views
{
	/// <summary>
	/// Interaction logic for UsersView.xaml
	/// </summary>
	public partial class UsersView : UserControl
	{
		public UsersView()
		{
			InitializeComponent();
			string serverHost = ConfigurationManager.AppSettings["serverHost"];
			int serverPort = int.Parse(ConfigurationManager.AppSettings["serverPort"]);
			int updatesPort = int.Parse(ConfigurationManager.AppSettings["updatesPort"]);
			DataContext = new UsersViewModel(serverHost, serverPort, updatesPort);
		}
	}
}
