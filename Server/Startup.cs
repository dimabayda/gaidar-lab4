﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Protocols;
using Server.Data;
using Server.Sockets;
using Swashbuckle.AspNetCore.Swagger;

namespace Server
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		public void ConfigureServices(IServiceCollection services)
		{
			services.AddDbContext<UsersContext>(options =>
				options.UseSqlServer(Configuration.GetConnectionString("UsersDatabase")));
			services.AddMvc();
			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("Users", new Info { Title = "Users API" });
			});
			services.AddSingleton(new UpdateCenter(IPAddress.Any, 5001));
		}

		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			app.UseMvc();
			app.UseSwagger();
			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/users.json", "Users API");
			});
		}
	}
}
