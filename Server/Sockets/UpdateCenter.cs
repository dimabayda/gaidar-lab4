﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Server.Sockets
{
	public class UpdateCenter
	{
		private TcpListener tcpListener;
		private List<TcpClient> clients = new List<TcpClient>();

		public UpdateCenter(IPAddress address, int port)
		{
			tcpListener = new TcpListener(address, port);
			ListenForClients();
		}

		public void NotifyClientsForChanges()
		{
			try
			{
				foreach (TcpClient client in clients)
				{
					Stream stream = client.GetStream();
					try
					{
						stream.WriteByte(155);
					}
					catch (Exception)
					{
						clients.Remove(client);
					}
				}
			}
			catch(Exception)
			{
			}
		}

		private async void ListenForClients()
		{
			tcpListener.Start();
			while (true)
			{
				TcpClient client = await tcpListener.AcceptTcpClientAsync();
				clients.Add(client);
			}
		}
	}
}
