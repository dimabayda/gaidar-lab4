﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Server.Data
{
	public class UsersContext : DbContext
	{
		public DbSet<User> Users { get; set; }

		public UsersContext(DbContextOptions options) : base(options)
		{

		}
	}
}
