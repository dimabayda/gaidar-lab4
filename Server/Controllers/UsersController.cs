﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Server.Data;
using Server.Sockets;
using Shared.Models;

namespace Server.Controllers
{
	[Route("api/[controller]")]
	public class UsersController : Controller
	{
		private UsersContext usersContext;
		private UpdateCenter updateCenter;

		public UsersController(UsersContext usersContext, UpdateCenter updateCenter)
		{
			this.usersContext = usersContext;
			this.updateCenter = updateCenter;
		}

		[HttpGet]
		public IEnumerable<User> Get()
		{
			return usersContext.Users;
		}

		[HttpGet("{id}")]
		public User Get(int id)
		{
			return usersContext.Users.FirstOrDefault(user => user.Id == id);
		}

		[HttpPost]
		public int Post(User user)
		{
			User newUser = new User
			{
				Name = user.Name,
				Birthday = user.Birthday
			};
			usersContext.Add(newUser);
			usersContext.SaveChanges();
			updateCenter.NotifyClientsForChanges();
			return newUser.Id;
		}

		[HttpPut]
		public void Put(User user)
		{
			User trackUser = usersContext.Users.FirstOrDefault(usr => usr.Id == user.Id);
			if (!trackUser.Equals(user))
			{
				trackUser.Birthday = user.Birthday;
				trackUser.Name = user.Name;
				usersContext.SaveChanges();
				updateCenter.NotifyClientsForChanges();
			}
		}

		[HttpDelete]
		public void Delete(int id)
		{
			usersContext.Users.RemoveRange(usersContext.Users.Where(user => user.Id == id));
			usersContext.SaveChanges();
			updateCenter.NotifyClientsForChanges();
		}
	}
}
