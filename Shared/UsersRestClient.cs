﻿using Newtonsoft.Json;
using RestSharp;
using Shared.Models;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
	public class UsersRestClient
	{
		private RestClient client;

		public UsersRestClient(string baseAddress)
		{
			client = new RestClient(baseAddress);
		}

		public User[] GetUsers()
		{
			RestRequest request = new RestRequest("api/users", Method.GET);
			string result = client.Execute(request).Content;
			return JsonConvert.DeserializeObject<User[]>(result);
		}

		public User GetUser(int id)
		{
			RestRequest request = new RestRequest("api/users/{id}", Method.GET);
			request.AddParameter("id", id);
			string result = client.Execute(request).Content;
			return JsonConvert.DeserializeObject<User>(result);
		}

		public int AddUser(User user)
		{
			RestRequest request = new RestRequest("api/users", Method.POST);
			request.AddObject(user);
			string result = client.Execute(request).Content;
			return int.Parse(result);
		}

		public void ChangeUser(User user)
		{
			RestRequest request = new RestRequest("api/users", Method.PUT);
			request.AddObject(user);
			client.Execute(request);
		}

		public void DeleteUser(User user)
		{
			RestRequest request = new RestRequest("api/users", Method.DELETE);
			request.AddParameter("id", user.Id);
			client.Execute(request);
		}
	}
}
