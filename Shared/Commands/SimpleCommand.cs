﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Shared.Commands
{
	public class SimpleCommand : ICommand
	{
		private readonly Action<object> action;

		public event EventHandler CanExecuteChanged;

		public SimpleCommand(Action<object> action)
		{
			this.action = action;
		}

		public bool CanExecute(object parameter) => true;

		public void Execute(object parameter) => action(parameter);
	}
}
