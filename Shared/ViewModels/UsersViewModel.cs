﻿using Shared.Commands;
using Shared.Models;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Shared.ViewModels
{
	public delegate void OnUpdateHandler(UsersViewModel usersViewModel);

	public class UsersViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		private UsersRestClient usersRestClient;
		private string host;

		public User[] Users { get; private set; }
		public SimpleCommand DeleteUserCommand { get; private set; }
		public SimpleCommand AddNewUserCommand { get; private set; }

		public UsersViewModel(string host, int port, int updatesPort)
		{
			this.host = host;
			DeleteUserCommand = new SimpleCommand(DeleteUser);
			AddNewUserCommand = new SimpleCommand(AddNewEmptyUser);
			usersRestClient = new UsersRestClient($"http://{host}:{port}");
			UpdateUsers();
			ListenForUpdates(updatesPort);
		}

		private async void ListenForUpdates(int updatesPort)
		{
			await Task.Run(() =>
			{
				TcpClient tcpClient = new TcpClient(host, updatesPort);
				var stream = tcpClient.GetStream();
				while (true)
				{
					while (stream.DataAvailable)
					{
						int bt = stream.ReadByte();
						UpdateUsers();
					}
				}
			});
		}

		private void UpdateUsers()
		{
			Users = usersRestClient.GetUsers();
			foreach(User user in Users)
			{
				user.PropertyChanged += User_PropertyChanged;
			}
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(null));
		}

		private void DeleteUser(object parameter)
		{
			User user = parameter as User;
			usersRestClient.DeleteUser(user);
		}

		private void AddNewEmptyUser(object parameter)
		{
			User user = new User();
			user.Id = usersRestClient.AddUser(user);
		}

		private void User_PropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			User user = sender as User;
			usersRestClient.ChangeUser(user);
		}
	}
}
