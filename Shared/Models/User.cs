﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Shared.Models
{
	public class User : INotifyPropertyChanged, IEquatable<User>
	{
		private int id;
		private string name;
		private DateTime birthday;

		public event PropertyChangedEventHandler PropertyChanged;

		public int Id
		{
			get => id; set
			{
				id = value;
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Id"));
			}
		}

		public string Name
		{
			get => name; set
			{
				name = value;
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Name"));
			}
		}

		public DateTime Birthday
		{
			get => birthday; set
			{
				birthday = value;
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Birthday"));
			}
		}

		public bool Equals(User other)
		{
			return Id == other.Id && Name == other.Name && Birthday == other.Birthday;
		}
	}
}
